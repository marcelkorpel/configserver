package nl.marcelkorpel.configserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ConfigServer
 */
@WebServlet("/")
public class ConfigServer extends HttpServlet {

  private static final long serialVersionUID = 1L;
  final private String APPROOT;
  final private String CONFIG_FILE;
  final private String APPNAME = "XXX";
  final private String FILENAME = "config";
  final private ArrayList<ConfigProperty> cfgProps;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ConfigServer() {
    super();
    this.cfgProps = new ArrayList<>();
    addProperty("db_driver", "Database driver");
    addProperty("db_dsn_prefix", "Database prefix");
    addProperty("db_host", "Database hostname");
    addProperty("db_port", "Database port");
    addProperty("db_name", "Database name");
    addProperty("db_username", "Database username");
    addProperty("db_password", "Database password");
    addProperty("smtp_hostname", "SMTP hostname");
    addProperty("smtp_username", "SMTP username");
    addProperty("smtp_password", "SMTP password");
    addProperty("goog_api_key", "Google API key");
    addProperty("goog_api_secret", "Google API secret");
    addProperty("fb_api_key", "Facebook API key");
    addProperty("fb_api_secret", "Facebook API secret");
    addProperty("ms_api_key", "Microsoft API key");
    addProperty("ms_api_secret", "Microsoft API secret");

    /* OpenShift workaround */
    if (System.getenv("OPENSHIFT_DATA_DIR") == null) {
      APPROOT = System.getProperty("user.home") + "/.app-data/" + APPNAME;
    } else {
      APPROOT = System.getenv("OPENSHIFT_DATA_DIR") + APPNAME;
    }
    File f = new File(APPROOT);
    f.mkdirs();
    CONFIG_FILE = APPROOT + "/" + FILENAME + ".properties";
  }

  /**
   * @param request
   * @param response
   * @throws javax.servlet.ServletException
   * @throws java.io.IOException
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   * response)
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Properties prop = new Properties();
    String message = null;
    InputStream in = null;
    response.setContentType("text/html; charset=UTF-8");

    File file = new File(CONFIG_FILE);
    if (!file.exists()) {
      new FileOutputStream(file).close();
    }

    try {
      in = new FileInputStream(CONFIG_FILE);
      prop.load(in);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    for (ConfigProperty cfgProp : cfgProps) {
      cfgProp.setValue(prop.getProperty(cfgProp.getName(), ""));
    }

    HttpSession session = request.getSession(false);
    if (session != null) {
      message = (String) session.getAttribute("message");
      session.invalidate();
    }

    request.setAttribute("cfgProps", cfgProps);
    request.setAttribute("message", message);
    request.getRequestDispatcher("/WEB-INF/form.xhtml").forward(request, response);
  }

  /**
   * @param request
   * @param response
   * @throws javax.servlet.ServletException
   * @throws java.io.IOException
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   * response)
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Properties prop = new Properties();
    OutputStream output = null;
    try {
      output = new FileOutputStream(CONFIG_FILE);

      for (ConfigProperty param : cfgProps) {
        String p = request.getParameter(param.getName());
        if (p == null) { // parameter not present
          // this shouldn't happen, unless someone tries to hack the servlet!
          PrintWriter out = response.getWriter();
          out.print("Bogus input!");
        } else {
          prop.setProperty(param.getName(), p);
        }
      }
      prop.store(output, null);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (output != null) {
        try {
          output.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    HttpSession session = request.getSession(true);
    session.setAttribute("message", "Configuration saved");

    response.setStatus(HttpServletResponse.SC_SEE_OTHER);
    response.setHeader("Location", request.getRequestURI());
  }

  private void addProperty(String name, String description) {
    cfgProps.add(new ConfigProperty(name, description));
  }
}
