package nl.marcelkorpel.configserver;

public class ConfigProperty {
  private String name;
  private String description;
  private String value;

  public ConfigProperty(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }
  
  public String getValue() {
    return value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
}
